package com.dipankar.kubernetes.external.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableDiscoveryClient
public class ExternalController {

    @Autowired
    private RestTemplate template;

    @GetMapping(value = "/kubernetes/external/message")
    public String getMessage(){
        System.out.println("Calling internal service from external app");
        String url = "http://internal-app:8080/kubernetes/internal/message";
        System.out.println(" New URL : " + url);
        String reposnoe = template.getForObject(url,String.class);
        return "Received message from internal service and message is : " +  reposnoe;
    }
}
