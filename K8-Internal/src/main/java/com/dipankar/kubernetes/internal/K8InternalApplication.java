package com.dipankar.kubernetes.internal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8InternalApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8InternalApplication.class, args);
	}

}
