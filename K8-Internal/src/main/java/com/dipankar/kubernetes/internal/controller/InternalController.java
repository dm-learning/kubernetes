package com.dipankar.kubernetes.internal.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InternalController {

    @GetMapping (value = "kubernetes/internal/message")
    public String getResponse(){
        System.out.println("Kubernetes internal service");
        return "From Internal Service";
    }
}
